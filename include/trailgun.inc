/** Double-include prevention */
#if defined _trailgun_included_
  #endinput
#endif
#define _trailgun_included_

/**
 * Does the player have the trailgun enabled?
 *
 * @param1		client - player's client index
 * @return        True if the player has it enabled, false otherwise
 */
native bool TG_Enabled(int client);

#if !defined REQUIRE_PLUGIN
public void __pl_trailgun_SetNTVOptional()
{
	MarkNativeAsOptional("TG_Enabled");
}
#endif